#include <stdio.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>

 
#define DIM 9
int values[DIM] = {1, 60, 9, 18, 91, 7, 26, 75, 6};

void print_max() {
	int max = 0;
	for (int i = 0; i < DIM; i++){
		if(max < values[i]) {
			max = values[i];
		}
	}
	printf("Maximum is %d\n", max);
}

void print_avg() {
	int sum = 0;
	for (int i = 0; i < DIM; i++){
		sum += values[i];
	}
	printf("Average is %f\n", (float)(sum) / DIM);
}

void print_values() {
	printf("Values: ");
	for (int i = 0; i < DIM; i++) {
		printf ("%d ", values[i]);
	}
	printf("\n");
}

int main() {

pid_t pid, pid2; 
 pid = fork();
 pid2 = fork();

 	if (pid == 0){
	print_avg();
	}
	
	else if (pid2 == 0){
	print_max();
	}
	
	else {
	print_values();
	wait(NULL);
	exit(0);
	}
}
